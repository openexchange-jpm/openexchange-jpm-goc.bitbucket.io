document.addEventListener('DOMContentLoaded', function() {
    var pageTop = document.getElementById("page-top");
    if (pageTop !== null) {
         /* ----------------------- add class to list-view-item ---------------------- */
        // get all listitems
        var listItems = document.querySelectorAll(".list-view-item");

        // recur through listitems
        for(var i = 0; i < listItems.length; i ++){
            
            // get each listItem
            var listItem = listItems[i];

            // get title of each item

            var tileTitle = listItem.querySelector('.tile-title > a');
            // for link list
            if(tileTitle){
                // add class .list-view-section to list-view-item if there is {section} in a title
                var detectTags = [
                    {
                        "tag": "{ppt}",
                        "class": "list-view-ppt",
                    },
                    {
                        "tag": "{disable}",
                        "class": "list-view-disable",
                    },
                ];
                addTagTitle(tileTitle, listItem, detectTags);
            }

        }

        document.getElementById("showcase_category").style.display = "block";
        
         /* ------------------- end adding class to list-view-item ------------------- */

    }
});

// this function is used to add tag to add class in list-view-item
function addTagTitle(tileTitle, listItem, detectTags){
    // get title text
     var tileTitleText = tileTitle.innerText.trim();

     for(var i = 0; i < detectTags.length; i ++){
        var detectTag = detectTags[i];
        console.log(detectTag.tag);
        // check if title text include {section} 
        if (tileTitleText.includes(detectTag.tag)) {

            listItem.classList.add(detectTag.class);

            // create a new title without {section} because {section} is not needed
            var newTitleText = tileTitleText.replace(detectTag.tag,"").trim();
            // replace the old title with the new title
            tileTitle.innerHTML = newTitleText;
        }

     }

    
}