document.addEventListener('DOMContentLoaded', function() {
    var pageTop = document.getElementById("page-top");
    if (pageTop !== null) {
        // load html
        fetch('https://bitbucket.org/openexchange-jpm/openexchange-jpm-goc.bitbucket.io/raw/master/faqs.html').then(function(response){
            return response.text();
        }).then(function(responseHTML){ // get html from url
            var showcaseCategoryRow = document.querySelector("#showcase_category > .container > .row")
            //put html in showcase Category div
            showcaseCategoryRow.innerHTML = responseHTML;
        }).catch(function(err){
            // There was an error
	        console.warn('Something went wrong.', err);
        });
    }
});


